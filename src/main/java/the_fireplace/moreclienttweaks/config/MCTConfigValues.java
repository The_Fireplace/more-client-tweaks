package the_fireplace.moreclienttweaks.config;
/**
 * 
 * @author The_Fireplace
 *
 */
public class MCTConfigValues {
	public static final boolean SKIN0_DEFAULT = false;
	public static boolean SKIN0;
	public static final String SKIN0_NAME = "cape_toggle";
	public static final boolean SKIN1_DEFAULT = true;
	public static boolean SKIN1;
	public static final String SKIN1_NAME = "jacket_toggle";
	public static final boolean SKIN2_DEFAULT = true;
	public static boolean SKIN2;
	public static final String SKIN2_NAME = "left_sleeve_toggle";
	public static final boolean SKIN3_DEFAULT = true;
	public static boolean SKIN3;
	public static final String SKIN3_NAME = "right_sleeve_toggle";
	public static final boolean SKIN4_DEFAULT = true;
	public static boolean SKIN4;
	public static final String SKIN4_NAME = "left_pants_leg_toggle";
	public static final boolean SKIN5_DEFAULT = true;
	public static boolean SKIN5;
	public static final String SKIN5_NAME = "right_pants_leg_toggle";
	public static final boolean SKIN6_DEFAULT = true;
	public static boolean SKIN6;
	public static final String SKIN6_NAME = "hat_toggle";
	public static final int NORMALBRIGHTNESS_DEFAULT = 1;
	public static int NORMALBRIGHTNESS;
	public static final String NORMALBRIGHTNESS_NAME = "normal_brightness";
	public static final boolean GLOWINGPLAYERS_DEFAULT = true;
	public static boolean GLOWINGPLAYERS;
	public static final String GLOWINGPLAYERS_NAME = "glowingplayers";
	public static final boolean GLOWINGMOBS_DEFAULT = true;
	public static boolean GLOWINGMOBS;
	public static final String GLOWINGMOBS_NAME = "glowingmobs";
}

package the_fireplace.moreclienttweaks.keybind;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;

import org.lwjgl.input.Keyboard;

import the_fireplace.moreclienttweaks.MoreClientTweaks;
/**
 * 
 * @author The_Fireplace
 *
 */
public class KeyHandlerMCT {
	//Key Index
	public static final int FULLBRIGHT = 0;
	public static final int SKINTOGGLE = 1;
	//Descriptions, use language file to localize later
	private static final String[] desc = 
		{"key.fullbright.desc", "key.skintoggle.desc"};
	//Default Key Values
	private static final int[] keyValues = 
		{Keyboard.KEY_F, Keyboard.KEY_C};
	private final KeyBinding[] keys;
	public KeyHandlerMCT(){
		keys = new KeyBinding[desc.length];
		for(int i = 0; i < desc.length; ++i){
			keys[i] = new KeyBinding(desc[i], keyValues[i], "key.moreclienttweaks.category");
			ClientRegistry.registerKeyBinding(keys[i]);
		}
	}

	@SubscribeEvent
	public void onKeyInput(KeyInputEvent event){
		if(keys[FULLBRIGHT].isPressed()){
			MoreClientTweaks.toggleFullbright();
		}
		if(keys[SKINTOGGLE].isPressed()){
			MoreClientTweaks.toggleEnabledParts();
		}
	}
}

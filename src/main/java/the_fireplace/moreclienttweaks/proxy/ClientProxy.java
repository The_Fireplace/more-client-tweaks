package the_fireplace.moreclienttweaks.proxy;

import net.minecraftforge.fml.common.FMLCommonHandler;
import the_fireplace.moreclienttweaks.keybind.KeyHandlerMCT;
/**
 * 
 * @author The_Fireplace
 *
 */
public class ClientProxy extends CommonProxy {
	@Override
	public void registerRenderers(){

	}
	@Override
	public void registerClientEvents(){
		FMLCommonHandler.instance().bus().register(new KeyHandlerMCT());
	}
}

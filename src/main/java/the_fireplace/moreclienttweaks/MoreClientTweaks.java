package the_fireplace.moreclienttweaks;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import the_fireplace.moreclienttweaks.config.MCTConfigValues;
import the_fireplace.moreclienttweaks.proxy.CommonProxy;
/**
 * 
 * @author The_Fireplace
 *
 */
@Mod(modid=MoreClientTweaks.MODID, name=MoreClientTweaks.MODNAME, guiFactory = "the_fireplace.moreclienttweaks.config.MCTGuiFactory", canBeDeactivated=true, clientSideOnly=true)
public class MoreClientTweaks {
	public static final String MODID = "moreclienttweaks";
	public static final String MODNAME = "More Client Tweaks";

	@SidedProxy(clientSide="the_fireplace.moreclienttweaks.proxy.ClientProxy", serverSide="the_fireplace.moreclienttweaks.proxy.CommonProxy")
	public static CommonProxy proxy;

	public static Configuration config;

	public static Property ENABLECAPETOGGLE_PROPERTY;
	public static Property ENABLEHATTOGGLE_PROPERTY;
	public static Property ENABLECHESTTOGGLE_PROPERTY;
	public static Property ENABLELEFTARMTOGGLE_PROPERTY;
	public static Property ENABLELEFTLEGTOGGLE_PROPERTY;
	public static Property ENABLERIGHTARMTOGGLE_PROPERTY;
	public static Property ENABLERIGHTLEGTOGGLE_PROPERTY;
	public static Property NORMALBRIGHTNESS_PROPERTY;
	public static Property GLOWINGPLAYERS_PROPERTY;
	public static Property GLOWINGMOBS_PROPERTY;

	public static void syncConfig(){
		MCTConfigValues.SKIN0 = ENABLECAPETOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN1 = ENABLECHESTTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN2 = ENABLELEFTARMTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN3 = ENABLERIGHTARMTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN4 = ENABLELEFTLEGTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN5 = ENABLERIGHTLEGTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.SKIN6 = ENABLEHATTOGGLE_PROPERTY.getBoolean();
		MCTConfigValues.NORMALBRIGHTNESS = NORMALBRIGHTNESS_PROPERTY.getInt();
		MCTConfigValues.GLOWINGMOBS = GLOWINGMOBS_PROPERTY.getBoolean();
		MCTConfigValues.GLOWINGPLAYERS = GLOWINGPLAYERS_PROPERTY.getBoolean();
		if(config.hasChanged()){
			config.save();
		}
	}
	@EventHandler
	public void PreInit(FMLPreInitializationEvent event){
		MinecraftForge.EVENT_BUS.register(new ForgeEvents());
		proxy.registerClientEvents();
		proxy.registerRenderers();
		config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		config.addCustomCategoryComment(Configuration.CATEGORY_GENERAL, "The enable toggle options control whether those parts of the skin toggle or not when you use the keybind.");
		ENABLECAPETOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN0_NAME, MCTConfigValues.SKIN0_DEFAULT, "Enable cape toggle?");
		ENABLECHESTTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN1_NAME, MCTConfigValues.SKIN1_DEFAULT, "Enable jacket toggle?");
		ENABLELEFTARMTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN2_NAME, MCTConfigValues.SKIN2_DEFAULT, "Enable left sleeve toggle?");
		ENABLERIGHTARMTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN3_NAME, MCTConfigValues.SKIN3_DEFAULT, "Enable right sleeve toggle?");
		ENABLELEFTLEGTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN4_NAME, MCTConfigValues.SKIN4_DEFAULT, "Enable left pant leg toggle?");
		ENABLERIGHTLEGTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN5_NAME, MCTConfigValues.SKIN5_DEFAULT, "Enable right pant leg toggle?");
		ENABLEHATTOGGLE_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.SKIN6_NAME, MCTConfigValues.SKIN6_DEFAULT, "Enable hat toggle?");
		NORMALBRIGHTNESS_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.NORMALBRIGHTNESS_NAME, MCTConfigValues.NORMALBRIGHTNESS_DEFAULT, "The normal brightness setting to revert to when high gamma mode is toggled off.");
		GLOWINGMOBS_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.GLOWINGMOBS_NAME, MCTConfigValues.GLOWINGMOBS_DEFAULT, "Do mobs appear to be glowing?");
		GLOWINGPLAYERS_PROPERTY = config.get(Configuration.CATEGORY_GENERAL, MCTConfigValues.GLOWINGPLAYERS_NAME, MCTConfigValues.GLOWINGPLAYERS_DEFAULT, "Do players appear to be glowing?");
		syncConfig();
	}
	/**
	 * Toggles fullbright
	 */
	public static void toggleFullbright(){
		if(Minecraft.getMinecraft().gameSettings.gammaSetting < 1000000){
			Minecraft.getMinecraft().gameSettings.gammaSetting = 1000000;
		}else{
			Minecraft.getMinecraft().gameSettings.gammaSetting = NORMALBRIGHTNESS_PROPERTY.getInt();
		}
		Minecraft.getMinecraft().gameSettings.saveOptions();
	}
	/**
	 * Toggles the parts of the skin set to toggle in the settings
	 */
	public static void toggleEnabledParts(){
		if(ENABLECAPETOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.CAPE);
		}
		if(ENABLECHESTTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.JACKET);
		}
		if(ENABLELEFTARMTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.LEFT_SLEEVE);
		}
		if(ENABLERIGHTARMTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.RIGHT_SLEEVE);
		}
		if(ENABLELEFTLEGTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.LEFT_PANTS_LEG);
		}
		if(ENABLERIGHTLEGTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.RIGHT_PANTS_LEG);
		}
		if(ENABLEHATTOGGLE_PROPERTY.getBoolean()){
			Minecraft.getMinecraft().gameSettings.switchModelPartEnabled(EnumPlayerModelParts.HAT);
		}
		Minecraft.getMinecraft().gameSettings.saveOptions();
	}
}

package the_fireplace.moreclienttweaks;

import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import the_fireplace.moreclienttweaks.config.MCTConfigValues;

/**
 * 
 * @author The_Fireplace
 *
 */
public class ForgeEvents {
	@SubscribeEvent
	public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
		if(eventArgs.getModID().equals(MoreClientTweaks.MODID))
			MoreClientTweaks.syncConfig();
	}
	@SubscribeEvent
	public void livingUpdate(LivingEvent.LivingUpdateEvent event){
		if(event.getEntityLiving() instanceof EntityPlayer && MCTConfigValues.GLOWINGPLAYERS && !event.getEntityLiving().isGlowing())
			event.getEntityLiving().setGlowing(true);
		else if(event.getEntityLiving() instanceof EntityMob && MCTConfigValues.GLOWINGMOBS && !event.getEntityLiving().isGlowing())
			event.getEntityLiving().setGlowing(true);
	}
}
